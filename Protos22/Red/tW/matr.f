      REAL*8 FUNCTION GB_TWM(P1,P2,P3a,P3b,P3c,P4a,P4b,NHEL)

!     P1 = g    P2 = b    P3 = t    P4 = W-

      IMPLICIT NONE

!     CONSTANTS

      INTEGER NGRAPHS,NEIGEN,NEXTERNAL
      PARAMETER (NGRAPHS=4,NEIGEN=1,NEXTERNAL=7)

!     ARGUMENTS 

      REAL*8 P1(0:3),P2(0:3),P3a(0:3),P3b(0:3),P3c(0:3),
     &  P4a(0:3),P4b(0:3)
      INTEGER NHEL(NEXTERNAL)

!     LOCAL VARIABLES 

      INTEGER I,J
      REAL*8 EIGEN_VAL(NEIGEN),EIGEN_VEC(NGRAPHS,NEIGEN)
      COMPLEX*16 ZTEMP
      COMPLEX*16 AMP(NGRAPHS)
      COMPLEX*16 W1(6),W2(6),W3(6),W4(6),W5(6),W6(6)
      COMPLEX*16 W3a(6),W3b(6),W3c(6),W3d(6)
      COMPLEX*16 W4a(6),W4b(6)

!     External data

      REAL*8 MZ,GZ,MW,GW,mt,Gt,mb,MH,GH,mtau,mc
      COMMON /SMMASS/ MZ,GZ,MW,GW,mt,Gt,MH,GH,mb,mc,mtau
      REAL*8 alpha,sw2,alpha_s,g,e,gs
      COMMON /SMCOUP/ alpha,sw2,alpha_s,g,e,gs
      COMPLEX*16 VL,VR,gL,gR
      COMMON /tcoup/ VL,VR,gL,gR
      REAL*8 x1,x2,s,Q
      INTEGER IDIR,IF1,IF2
      COMMON /miscdata/ x1,x2,s,Q,IDIR,IF1,IF2

!     Couplings and other

      REAL*8 GWF(2),GG(2)
      COMPLEX*16 GWtb_p(2),GWtb_d(2),G2Wtb(2),G2Wbt(2)
      COMPLEX*16 Wdum(6)
      REAL*8 F1MASS,F2MASS

!     Flags

      INTEGER IAN_PROD,IAN_DEC
      COMMON /ANFLAGS/ IAN_PROD,IAN_DEC

!     COLOR DATA

      DATA EIGEN_VAL(1) /0.1666666666666666d0/
      DATA EIGEN_VEC(1,1) /-1d0/
      DATA EIGEN_VEC(2,1) /-1d0/
      DATA EIGEN_VEC(3,1) /-1d0/
      DATA EIGEN_VEC(4,1) /-1d0/

      GWF(1)=-g/SQRT(2d0)
      GWF(2)=0d0

      GG(1)=-gs
      GG(2)=-gs

      GWtb_p(1)=GWF(1)*CONJG(VL)     ! t production: b in t out
      GWtb_p(2)=GWF(1)*CONJG(VR)
      GWtb_d(1)=GWF(1)*VL            ! t decay: t in b out
      GWtb_d(2)=GWF(1)*VR

      G2Wtb(1)=GWF(1)/MW*gL          ! t in b out
      G2Wtb(2)=GWF(1)/MW*gR
      G2Wbt(1)=-GWF(1)/MW*CONJG(gR)  ! b in t out
      G2Wbt(2)=-GWF(1)/MW*CONJG(gL)

      F1MASS=0d0
      IF (IF1 .EQ. 3) F1MASS=mtau
      F2MASS=0d0
      IF (IF2 .EQ. 3) F2MASS=mtau

      IF (IAN_PROD .EQ. 0) THEN
        GWtb_p(1)=GWF(1)
        GWtb_p(2)=0d0
      ENDIF
      IF (IAN_DEC .EQ. 0) THEN
        GWtb_d(1)=GWF(1)
        GWtb_d(2)=0d0
      ENDIF

!     Code

      CALL VXXXXX(P1,0d0,NHEL(1),-1,W1)     ! g
      CALL IXXXXX(P2,0d0,NHEL(2),1,W2)      ! b

      CALL OXXXXX(P3a,0d0,NHEL(3),1,W3a)            ! nu / q
      CALL IXXXXX(P3b,F1MASS,NHEL(4),-1,W3b)        ! e+ / qbar
      CALL OXXXXX(P3c,mb,NHEL(5),1,W3c)             ! b
      CALL JIOXXX(W3b,W3a,GWF,MW,GW,W3d)            ! W+
      CALL FVOCXX(W3c,W3d,GWtb_d,mt,Gt,W3)            ! t
      IF (IAN_DEC .EQ. 1) THEN
        CALL FVOSmX(W3c,W3d,G2Wtb,mt,Gt,1,Wdum)       ! t anomalous
        W3(1)=W3(1)+Wdum(1)
        W3(2)=W3(2)+Wdum(2)
        W3(3)=W3(3)+Wdum(3)
        W3(4)=W3(4)+Wdum(4)
      ENDIF

      CALL IXXXXX(P4a,0d0,NHEL(6),-1,W4a)               ! nu~ / qbar
      CALL OXXXXX(P4b,F2MASS,NHEL(7),1,W4b)             ! e-  / q
      CALL JIOXXX(W4a,W4b,GWF,MW,GW,W4)                 ! W-

      CALL FVOXXX(W3,W1,GG,mt,Gt,W5)
      CALL IOVCXX(W2,W5,W4,GWtb_p,AMP(1))

      CALL FVIXXX(W2,W1,GG,mb,0d0,W6)
      CALL IOVCXX(W6,W3,W4,GWtb_p,AMP(3))

      IF (IAN_PROD .EQ. 1) THEN
        CALL IOVSmX(W2,W5,W4,G2Wbt,-1,AMP(2))
        CALL IOVSmX(W6,W3,W4,G2Wbt,-1,AMP(4))
      ELSE
        AMP(2)=0d0
        AMP(4)=0d0
      ENDIF

      GB_TWM = 0d0 
      DO I = 1, NEIGEN
          ZTEMP = (0d0,0d0)
          DO J = 1, NGRAPHS
              ZTEMP = ZTEMP + EIGEN_VEC(J,I)*AMP(J)
          ENDDO
          GB_TWM =GB_TWM + ZTEMP*EIGEN_VAL(I)*CONJG(ZTEMP)
      ENDDO
      END



      REAL*8 FUNCTION GBB_TBWP(P1,P2,P3a,P3b,P3c,P4a,P4b,NHEL)

!     P1 = g    P2 = b~    P3 = t~    P4 = W+

      IMPLICIT NONE

!     CONSTANTS

      INTEGER NGRAPHS,NEIGEN,NEXTERNAL
      PARAMETER (NGRAPHS=4,NEIGEN=1,NEXTERNAL=7)

!     ARGUMENTS 

      REAL*8 P1(0:3),P2(0:3),P3a(0:3),P3b(0:3),P3c(0:3),
     &  P4a(0:3),P4b(0:3)
      INTEGER NHEL(NEXTERNAL)

!     LOCAL VARIABLES 

      INTEGER I,J
      REAL*8 EIGEN_VAL(NEIGEN),EIGEN_VEC(NGRAPHS,NEIGEN)
      COMPLEX*16 ZTEMP
      COMPLEX*16 AMP(NGRAPHS)
      COMPLEX*16 W1(6),W2(6),W3(6),W4(6),W5(6),W6(6)
      COMPLEX*16 W3a(6),W3b(6),W3c(6),W3d(6)
      COMPLEX*16 W4a(6),W4b(6)

!     External data

      REAL*8 MZ,GZ,MW,GW,mt,Gt,mb,MH,GH,mtau,mc
      COMMON /SMMASS/ MZ,GZ,MW,GW,mt,Gt,MH,GH,mb,mc,mtau
      REAL*8 alpha,sw2,alpha_s,g,e,gs
      COMMON /SMCOUP/ alpha,sw2,alpha_s,g,e,gs
      COMPLEX*16 VL,VR,gL,gR
      COMMON /tcoup/ VL,VR,gL,gR
      REAL*8 x1,x2,s,Q
      INTEGER IDIR,IF1,IF2
      COMMON /miscdata/ x1,x2,s,Q,IDIR,IF1,IF2

!     Couplings and other

      REAL*8 GWF(2),GG(2)
      COMPLEX*16 GWtb_p(2),GWtb_d(2),G2Wtb(2),G2Wbt(2)
      COMPLEX*16 Wdum(6)
      REAL*8 F1MASS,F2MASS

!     Flags

      INTEGER IAN_PROD,IAN_DEC
      COMMON /ANFLAGS/ IAN_PROD,IAN_DEC


!     COLOR DATA

      DATA EIGEN_VAL(1) /0.1666666666666666d0/
      DATA EIGEN_VEC(1,1) /1d0/
      DATA EIGEN_VEC(2,1) /1d0/
      DATA EIGEN_VEC(3,1) /1d0/
      DATA EIGEN_VEC(4,1) /1d0/

      GWF(1)=-g/SQRT(2d0)
      GWF(2)=0d0

      GG(1)=-gs
      GG(2)=-gs

      GWtb_p(1)=GWF(1)*VL            ! tbar production: t in b out
      GWtb_p(2)=GWF(1)*VR
      GWtb_d(1)=GWF(1)*CONJG(VL)     ! tbar decay: b in t out
      GWtb_d(2)=GWF(1)*CONJG(VR)

      G2Wtb(1)=GWF(1)/MW*gL          ! t in b out
      G2Wtb(2)=GWF(1)/MW*gR
      G2Wbt(1)=-GWF(1)/MW*CONJG(gR)  ! b in t out
      G2Wbt(2)=-GWF(1)/MW*CONJG(gL)

      F1MASS=0d0
      IF (IF1 .EQ. 3) F1MASS=mtau
      F2MASS=0d0
      IF (IF2 .EQ. 3) F2MASS=mtau

      IF (IAN_PROD .EQ. 0) THEN
        GWtb_p(1)=GWF(1)
        GWtb_p(2)=0d0
      ENDIF
      IF (IAN_DEC .EQ. 0) THEN
        GWtb_d(1)=GWF(1)
        GWtb_d(2)=0d0
      ENDIF

!     Code

      CALL VXXXXX(P1,0d0,NHEL(1),-1,W1)      ! g
      CALL OXXXXX(P2,0d0,NHEL(2),-1,W2)      ! b~

      CALL IXXXXX(P3a,0d0,NHEL(3),-1,W3a)               ! nu~ / qbar
      CALL OXXXXX(P3b,F1MASS,NHEL(4),1,W3b)             ! e-  / q
      CALL IXXXXX(P3c,mb,NHEL(5),-1,W3c)                ! b~
      CALL JIOXXX(W3a,W3b,GWF,MW,GW,W3d)                ! W-
      CALL FVICXX(W3c,W3d,GWtb_d,mt,Gt,W3)                ! t~
      IF (IAN_DEC .EQ. 1) THEN
        CALL FVISmX(W3c,W3d,G2Wbt,mt,Gt,-1,Wdum)          ! t~ anom
        W3(1)=W3(1)+Wdum(1)
        W3(2)=W3(2)+Wdum(2)
        W3(3)=W3(3)+Wdum(3)
        W3(4)=W3(4)+Wdum(4)
      ENDIF

      CALL OXXXXX(P4a,0d0,NHEL(6),1,W4a)            ! nu / q
      CALL IXXXXX(P4b,F2MASS,NHEL(7),-1,W4b)        ! e+ / qbar
      CALL JIOXXX(W4b,W4a,GWF,MW,GW,W4)             ! W+

      CALL FVIXXX(W3,W1,GG,mt,Gt,W5)
      CALL IOVCXX(W5,W2,W4,GWtb_p,AMP(1))

      CALL FVOXXX(W2,W1,GG,mb,0d0,W6)
      CALL IOVCXX(W3,W6,W4,GWtb_p,AMP(3))

      IF (IAN_PROD .EQ. 1) THEN
        CALL IOVSmX(W5,W2,W4,G2Wtb,1,AMP(2))
        CALL IOVSmX(W3,W6,W4,G2Wtb,1,AMP(4))
      ELSE
        AMP(2)=0d0
        AMP(4)=0d0
      ENDIF

      GBB_TBWP = 0d0 
      DO I = 1, NEIGEN
          ZTEMP = (0d0,0d0)
          DO J = 1, NGRAPHS
              ZTEMP = ZTEMP + EIGEN_VEC(J,I)*AMP(J)
          ENDDO
          GBB_TBWP =GBB_TBWP+ZTEMP*EIGEN_VAL(I)*CONJG(ZTEMP) 
      ENDDO
      END
