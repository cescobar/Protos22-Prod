      SUBROUTINE VEGAS(FXN,AVGI,SD,CHI2A)
C
C   SUBROUTINE PERFORMS N-DIMENSIONAL MONTE CARLO INTEG'N
C      - BY G.P. LEPAGE   SEPT 1976/(REV)APR 1978
C
      IMPLICIT NONE

      INTEGER MAXGR
      PARAMETER (MAXGR=50)

      INTEGER MAXDIM,NDMX
      REAL*8 ALPH,ONE
      PARAMETER (MAXDIM=50,NDMX=50,ALPH=1.5,ONE=1d0)

      INTEGER NDIM,NCALL,ITMX,NPRN
      REAL*8 XL(MAXDIM),XU(MAXDIM),ACC
      COMMON/BVEG1/XL,XU,ACC,NDIM,NCALL,ITMX,NPRN

      INTEGER ndo,IT
      REAL*8 XI(MAXGR,50,MAXDIM),SI,SI2,SWGT,SCHI
      COMMON/BVEG2/XI,SI,SI2,SWGT,SCHI,ndo,IT

      INTEGER KG(MAXDIM),IA(MAXGR,MAXDIM)
      REAL*8 D(MAXGR,50,MAXDIM),DI(MAXGR,50,MAXDIM),XIN(MAXGR,50),
     &  R(MAXGR,50),DX(MAXDIM),DT(MAXGR,MAXDIM),X(MAXDIM)

      INTEGER INITGRID,IGR
      COMMON /multigr/ INITGRID,IGR
      REAL*8 GRWGT(MAXGR)
      COMMON /multigr2/ GRWGT

!     Variables not originally declared

!     Equal for all grids

      INTEGER I,J,JGR
      INTEGER mds,ND,NDM,NG,NPG,K
      REAL*8 CALLS,DR,RC,xo,XN
      REAL*8 F,F2
      REAL*8 XND,DXG,DV2G,XJAC
      REAL*8 AVGI,SD,CHI2A,WGT

!     Grid-dependent? I don't think so

      REAL*8 FB,F2B,TI,TI2,TSI

      REAL*8 FXN
      EXTERNAL FXN

      REAL*8 QRAN(MAXDIM)
      SAVE /BVEG1/,/BVEG2/,mds
      
!
      MDS=1
      ndo=1
      DO j=1,NDIM
        XI(IGR,1,j)=1d0
      ENDDO
!
      ENTRY VEGAS1(FXN,AVGI,SD,CHI2A)
!         - INITIALIZES CUMMULATIVE VARIABLES, BUT NOT GRID

      IT=0
      SI=0d0
      SI2=0d0
      SWGT=0d0
      SCHI=0d0
!
      ENTRY VEGAS2(FXN,AVGI,SD,CHI2A)
!         - NO INITIALIZATION
      ND=NDMX
      NG=1
      IF (MDS .NE. 0) THEN
        NG=(NCALL/2d0+0.25)**(1d0/NDIM)
        MDS=1
        IF ((2*ng-NDMX) .GE. 0) THEN
          MDS=-1
          NPG=NG/NDMX+1
          ND=NG/NPG
          NG=NPG*ND
        ENDIF
      ENDIF
      K=NG**NDIM
      NPG=NCALL/K
      IF(NPG.LT.2) NPG=2
      CALLS=NPG*K
      DXG=ONE/NG
      DV2G=(CALLS*DXG**NDIM)**2/NPG/NPG/(NPG-ONE)
      XND=ND
      NDM=ND-1
      DXG=DXG*XND
      XJAC=ONE/CALLS
      DO 3 J=1,NDIM
      DX(J)=XU(J)-XL(J)
3     XJAC=XJAC*DX(J)

C
C   REBIN PRESERVING BIN DENSITY
C
      IF(ND.EQ.ndo) GO TO 8
      RC=ndo/XND
      DO 7 J=1,NDIM
      K=0
      XN=0.
      DR=XN
      I=K
4     K=K+1
      DR=DR+ONE
      xo=XN
      XN=XI(IGR,K,J)
5     IF(RC.GT.DR) GO TO 4
      I=I+1
      DR=DR-RC
      XIN(IGR,I)=XN-(XN-xo)*DR
      IF(I.LT.NDM) GO TO 5
      DO 6 I=1,NDM
6     XI(IGR,I,J)=XIN(IGR,I)
7     XI(IGR,ND,J)=ONE
      ndo=ND
C
8     IF(NPRN.NE.0) WRITE(6,200) NDIM,CALLS,IT,ITMX,ACC,MDS,ND
!     1                           ,(XL(J),XU(J),J=1,NDIM)
C
      ENTRY VEGAS3(FXN,AVGI,SD,CHI2A)
C         - MAIN INTEGRATION LOOP
9     IT=IT+1
      TI=0.
      TSI=TI
      DO 10 J=1,NDIM
      KG(J)=1
      DO 10 I=1,ND
      DO 10 JGR=1,MAXGR                              ! JAAS - Multigrid

      D(JGR,I,J)=TI
10    DI(JGR,I,J)=TI
C
11    FB=0.
      F2B=FB
      K=0
12    K=K+1
      CALL ARAN9(QRAN,NDIM)
      WGT=XJAC
      IF (INITGRID .EQ. 0) WGT=WGT*GRWGT(IGR)        ! JAAS - Multigrid
      DO 15 J=1,NDIM
      XN=(KG(J)-QRAN(J))*DXG+ONE
      IA(IGR,J)=XN
      IF(IA(IGR,J).GT.1) GO TO 13
      xo=XI(IGR,IA(IGR,J),J)
      RC=(XN-IA(IGR,J))*xo
      GO TO 14
13    xo=XI(IGR,IA(IGR,J),J)-XI(IGR,IA(IGR,J)-1,J)
      RC=XI(IGR,IA(IGR,J)-1,J)+(XN-IA(IGR,J))*xo
14    X(J)=XL(J)+RC*DX(J)

15    WGT=WGT*xo*XND
C
      F=WGT
      F=F*FXN(X,WGT)
      F2=F*F
      FB=FB+F
      F2B=F2B+F2
      DO 16 J=1,NDIM
      DI(IGR,IA(IGR,J),J)=DI(IGR,IA(IGR,J),J)+F
16    IF(MDS.GE.0) D(IGR,IA(IGR,J),J)=D(IGR,IA(IGR,J),J)+F2
      IF(K.LT.NPG) GO TO 12
C
      F2B=SQRT(F2B*NPG)
      F2B=(F2B-FB)*(F2B+FB)
      TI=TI+FB
      TSI=TSI+F2B
      IF(MDS.GE.0) GO TO 18
      DO 17 J=1,NDIM
17    D(IGR,IA(IGR,J),J)=D(IGR,IA(IGR,J),J)+F2B
18    K=NDIM
19    KG(K)=MOD(KG(K),NG)+1
      IF(KG(K).NE.1) GO TO 11
      K=K-1
      IF(K.GT.0) GO TO 19
C
C   FINAL RESULTS FOR THIS ITERATION
C
      TSI=TSI*DV2G
      TI2=TI*TI
      WGT=TI2/TSI
      SI=SI+TI*WGT
      SI2=SI2+TI2
      SWGT=SWGT+WGT
      SCHI=SCHI+TI2*WGT
      AVGI=SI/SWGT
      SD=SWGT*IT/SI2
      CHI2A=SD*(SCHI/SWGT-AVGI*AVGI)/(IT-.999)
      SD=SQRT(ONE/SD)
C
      IF(NPRN.EQ.0) GO TO 21
      TSI=SQRT(TSI)
      WRITE(6,201) IT,TI,TSI,AVGI,SD,CHI2A
      IF(NPRN.GE.0) GO TO 21
      DO 20 J=1,NDIM
20    WRITE(6,202) J,(XI(IGR,I,J),DI(IGR,I,J),D(IGR,I,J),I=1,ND)
C
C   REFINE GRID
C


21    CONTINUE

c      IF (INITGRID .EQ. 0) GOTO 29         ! JAAS - Multigrid

      DO 23 J=1,NDIM
      xo=D(IGR,1,J)
      XN=D(IGR,2,J)
      D(IGR,1,J)=(xo+XN)/2.
      DT(IGR,J)=D(IGR,1,J)
      DO 22 I=2,NDM
      D(IGR,I,J)=xo+XN
      xo=XN
      XN=D(IGR,I+1,J)
      D(IGR,I,J)=(D(IGR,I,J)+XN)/3.
22    DT(IGR,J)=DT(IGR,J)+D(IGR,I,J)
      D(IGR,ND,J)=(XN+xo)/2.
23    DT(IGR,J)=DT(IGR,J)+D(IGR,ND,J)
C
      DO 28 J=1,NDIM
      RC=0.
      DO 24 I=1,ND
      R(IGR,I)=0.
      IF(D(IGR,I,J).LE.0.) GO TO 24
      xo=DT(IGR,J)/D(IGR,I,J)
      R(IGR,I)=((xo-ONE)/xo/LOG(xo))**ALPH
24    RC=RC+R(IGR,I)
      RC=RC/XND
      K=0
      XN=0.
      DR=XN
      I=K
25    K=K+1
      DR=DR+R(IGR,K)
      xo=XN
      XN=XI(IGR,K,J)
26    IF(RC.GT.DR) GO TO 25
      I=I+1
      DR=DR-RC
      XIN(IGR,I)=XN-(XN-xo)*DR/R(IGR,K)
      IF(I.LT.NDM) GO TO 26
      DO 27 I=1,NDM
27    XI(IGR,I,J)=XIN(IGR,I)
28    XI(IGR,ND,J)=ONE

29    CONTINUE                                  ! JAAS - Multigrid

      IF(IT.LT.ITMX.AND.ACC*ABS(AVGI).LT.SD) GO TO 9
200   FORMAT(//' INPUT PARAMETERS FOR VEGAS:  NDIM=',I3,'  NCALL=',F8.0
     1    /28X,'  IT=',I5,'  ITMX=',I5/28X,'  ACC=',G9.3
     2    /28X,'  MDS=',I3,'   ND=',I4/28X)  ! ,'  (XL,XU)=',
!     3    (T40,'( ',G12.6,' , ',G12.6,' )'))
201   FORMAT(/' INTEGRATION BY VEGAS' / ' ITERATION NO.',I3,
     1    ':   INTEGRAL =',G14.8/21X,'STD DEV  =',G10.4 /
     2    ' ACCUMULATED RESULTS:   INTEGRAL =',G14.8 /
     3    24X,'STD DEV  =',G10.4 / 24X,'CHI**2 PER IT''N =',G10.4)
202   FORMAT('0DATA FOR AXIS',I2 / ' ',6X,'X',7X,'  DELT I  ',
     1    2X,' CONV''CE  ',11X,'X',7X,'  DELT I  ',2X,' CONV''CE  '
     2   ,11X,'X',7X,'  DELT I  ',2X,' CONV''CE  ' /
     2    (' ',3G12.4,5X,3G12.4,5X,3G12.4))
      RETURN
      END


      SUBROUTINE SAVE(NDIM)
      IMPLICIT REAL*8(A-H,O-Z)
      PARAMETER (MAXGR=50)
      PARAMETER (MAXDIM=50)
      REAL*8 XI(MAXGR,50,MAXDIM),SI,SI2,SWGT,SCHI
      COMMON/BVEG2/XI,SI,SI2,SWGT,SCHI,ndo,IT
      SAVE /BVEG2/

C
C   STORES VEGAS DATA (UNIT 7) FOR LATER RE-INITIALIZATION
C
      WRITE(33,200) ndo,IT,SI,SI2,SWGT,SCHI,
     1             (((XI(JGR,I,J),JGR=1,MAXGR),I=1,ndo),J=1,NDIM)
      RETURN
      ENTRY RESTR(NDIM)
C
C   ENTERS INITIALIZATION DATA FOR VEGAS
C
      READ(33,200) ndo,IT,SI,SI2,SWGT,SCHI,
     1            (((XI(JGR,I,J),JGR=1,MAXGR),I=1,ndo),J=1,NDIM)
200   FORMAT(2I8,4Z16/(5Z16))
      RETURN
      END
 
      SUBROUTINE ARAN9(QRAN,NDIM)
      PARAMETER (MAXDIM=50)
      REAL*8 QRAN(MAXDIM),RAN2

      INTEGER INITGRID,IGR
      COMMON /multigr/ INITGRID,IGR

      PARAMETER (MAXGR=50)
      REAL*8 GRAPROB(0:MAXGR)
      COMMON /multigr3/ GRAPROB

      INTEGER idum
      COMMON /ranno/ idum

      IF (INITGRID .EQ. 0) THEN
      RCH=ran2(idum)
      IGR=0
      DO WHILE (GRAPROB(IGR) .LT. RCH)
        IGR=IGR+1
      END DO
      ENDIF

      DO I=1,NDIM
         QRAN(I)=RAN2(idum)
      enddo
      END
