      SUBROUTINE PHASE2(M0,M1,M2,P0,P1,P2,WGT)
      IMPLICIT NONE
      REAL*8 M0,M1,M2,P0(0:3),P1(0:3),P2(0:3)
      REAL*8 pi,fact,XRN
      REAL*8 E1,q1,phi1,sinth1,costh1,WGT
      REAL*8 P1cm(0:3),P2cm(0:3)
      PARAMETER (pi=3.1415926535,fact=5.10158806d-5)     ! 1/(2Pi)^4/(4Pi)

      E1=M0/2. + (M1**2-M2**2)/(2.*M0)    ! E of particle 1 in CM frame
      q1=SQRT(E1**2-M1**2)                 ! common 3-momentum of 1 and 2
      WGT=q1/M0*fact

      phi1=2.*pi*XRN(0)                   ! Generate Omega uniformly
      costh1=2.*XRN(0)-1.
      sinth1=SQRT(1.-costh1**2)

      P1cm(0)=E1                          ! Calculate momenta in P0 cm frame
      P1cm(1)=q1*sinth1*COS(phi1)
      P1cm(2)=q1*sinth1*SIN(phi1)
      P1cm(3)=q1*costh1
      P2cm(0)=M0-E1
      P2cm(1)=-P1cm(1)      
      P2cm(2)=-P1cm(2)      
      P2cm(3)=-P1cm(3)
      CALL BOOST(P0,P1cm,P1)              ! Boost
      CALL BOOST(P0,P2cm,P2)
      RETURN
      END



      SUBROUTINE PHASE2t(M0,M1,M2,P0,P1,P2,WGT,IDIR)
      IMPLICIT NONE
      REAL*8 M0,M1,M2,P0(0:3),P1(0:3),P2(0:3)
      REAL*8 pi,fact,XRN
      REAL*8 E1,q1,phi1,sinth1,costh1,WGT
      REAL*8 P1cm(0:3),P2cm(0:3)
      REAL*8 t0,t1,t,rho,WTj
      INTEGER IDIR
      PARAMETER (pi=3.1415926535,fact=5.10158806d-5)     ! 1/(2Pi)^4/(4Pi)
      REAL*8 a,nu
      PARAMETER (a=23.04d0,nu=0.9)

      E1=M0/2. + (M1**2-M2**2)/(2.*M0)    ! E of particle 1 in CM frame
      q1=SQRT(E1**2-M1**2)                 ! common 3-momentum of 1 and 2
      WGT=q1/M0*fact

      phi1=2.*pi*XRN(0)                   ! Generate phi uniformly
      t0=-2d0*E1*M0
      t1=0d0
      rho=XRN(0)
      t=a-(rho*(a-t1)**(1d0-nu)+(1d0-rho)*(a-t0)**(1d0-nu))**
     &  (1d0/(1d0-nu))

      costh1=1d0+t/(E1*M0)
      IF (IDIR .EQ. 1) costh1=-costh1

      WTj = -((a-t1)**(1d0-nu)-(a-t0)**(1d0-nu))/(1d0-nu)
      WGT=WGT*WTj*(a-t)**nu/(E1*M0)/2d0
c      costh1=2.*XRN(0)-1.
      sinth1=SQRT(1.-costh1**2)

      P1cm(0)=E1                          ! Calculate momenta in P0 cm frame
      P1cm(1)=q1*sinth1*COS(phi1)
      P1cm(2)=q1*sinth1*SIN(phi1)
      P1cm(3)=q1*costh1
      P2cm(0)=M0-E1
      P2cm(1)=-P1cm(1)      
      P2cm(2)=-P1cm(2)      
      P2cm(3)=-P1cm(3)
      CALL BOOST(P0,P1cm,P1)              ! Boost
      CALL BOOST(P0,P2cm,P2)
      RETURN
      END



      SUBROUTINE PHASE2sym(M0,M1,M2,P0,P1,P2,WGT)
      IMPLICIT NONE
      REAL*8 M0,M1,M2,P0(0:3),P1(0:3),P2(0:3)
      REAL*8 pi,fact,XRN,RAN2
      REAL*8 E1,q1,phi1,sinth1,costh1,WGT
      REAL*8 P1cm(0:3),P2cm(0:3)
      INTEGER idum
      COMMON /ranno/ idum
      PARAMETER (pi=3.1415926535,fact=5.10158806d-5)     ! 1/(2Pi)^4/(4Pi)

      E1=M0/2. + (M1**2-M2**2)/(2.*M0)    ! E of particle 1 in CM frame
      q1=SQRT(E1**2-M1**2)                 ! common 3-momentum of 1 and 2
      WGT=q1/M0*fact

      phi1=2.*pi*RAN2(idum)                   ! Generate Omega uniformly
      costh1=2.*XRN(0)-1.
      sinth1=SQRT(1.-costh1**2)

      P1cm(0)=E1                          ! Calculate momenta in P0 cm frame
      P1cm(1)=q1*sinth1*COS(phi1)
      P1cm(2)=q1*sinth1*SIN(phi1)
      P1cm(3)=q1*costh1
      P2cm(0)=M0-E1
      P2cm(1)=-P1cm(1)      
      P2cm(2)=-P1cm(2)      
      P2cm(3)=-P1cm(3)
      CALL BOOST(P0,P1cm,P1)              ! Boost
      CALL BOOST(P0,P2cm,P2)
      RETURN
      END



      SUBROUTINE PHASE2iso(M0,M1,M2,P0,P1,P2,WGT)
      IMPLICIT NONE
      REAL*8 M0,M1,M2,P0(0:3),P1(0:3),P2(0:3)
      REAL*8 pi,fact,RAN2
      REAL*8 E1,q1,phi1,sinth1,costh1,WGT
      REAL*8 P1cm(0:3),P2cm(0:3)
      INTEGER idum
      COMMON /ranno/ idum
      PARAMETER (pi=3.1415926535,fact=5.10158806d-5)     ! 1/(2Pi)^4/(4Pi)

      E1=M0/2. + (M1**2-M2**2)/(2.*M0)    ! E of particle 1 in CM frame
      q1=SQRT(E1**2-M1**2)                 ! common 3-momentum of 1 and 2
      WGT=q1/M0*fact

      phi1=2.*pi*RAN2(idum)                   ! Generate Omega uniformly
      costh1=2.*RAN2(idum)-1.
      sinth1=SQRT(1.-costh1**2)

      P1cm(0)=E1                          ! Calculate momenta in P0 cm frame
      P1cm(1)=q1*sinth1*COS(phi1)
      P1cm(2)=q1*sinth1*SIN(phi1)
      P1cm(3)=q1*costh1
      P2cm(0)=M0-E1
      P2cm(1)=-P1cm(1)      
      P2cm(2)=-P1cm(2)      
      P2cm(3)=-P1cm(3)
      CALL BOOST(P0,P1cm,P1)              ! Boost
      CALL BOOST(P0,P2cm,P2)
      RETURN
      END




      SUBROUTINE PHASE2tsym(M0,M1,M2,P0,P1,P2,WGT,IDIR)
      IMPLICIT NONE
      REAL*8 M0,M1,M2,P0(0:3),P1(0:3),P2(0:3)
      REAL*8 pi,fact,XRN,RAN2
      REAL*8 E1,q1,phi1,sinth1,costh1,WGT
      REAL*8 P1cm(0:3),P2cm(0:3)
      REAL*8 t0,t1,t,rho,WTj
      INTEGER IDIR
      INTEGER idum
      COMMON /ranno/ idum
      PARAMETER (pi=3.1415926535,fact=5.10158806d-5)     ! 1/(2Pi)^4/(4Pi)
      REAL*8 a,nu
      PARAMETER (a=6462.5521d0,nu=0.9)

      E1=M0/2. + (M1**2-M2**2)/(2.*M0)    ! E of particle 1 in CM frame
      q1=SQRT(E1**2-M1**2)                 ! common 3-momentum of 1 and 2
      WGT=q1/M0*fact

      phi1=2.*pi*RAN2(idum)                   ! Generate phi uniformly
      t0=-2d0*E1*M0
      t1=0d0
      rho=XRN(0)
      t=a-(rho*(a-t1)**(1d0-nu)+(1d0-rho)*(a-t0)**(1d0-nu))**
     &  (1d0/(1d0-nu))

      costh1=1d0+t/(E1*M0)
      IF (IDIR .EQ. 1) costh1=-costh1
      WTj = -((a-t1)**(1d0-nu)-(a-t0)**(1d0-nu))/(1d0-nu)
      WGT=WGT*WTj*(a-t)**nu/(E1*M0)/2d0
c      costh1=2.*XRN(0)-1.
      sinth1=SQRT(1.-costh1**2)

      P1cm(0)=E1                          ! Calculate momenta in P0 cm frame
      P1cm(1)=q1*sinth1*COS(phi1)
      P1cm(2)=q1*sinth1*SIN(phi1)
      P1cm(3)=q1*costh1
      P2cm(0)=M0-E1
      P2cm(1)=-P1cm(1)      
      P2cm(2)=-P1cm(2)      
      P2cm(3)=-P1cm(3)
      CALL BOOST(P0,P1cm,P1)              ! Boost
      CALL BOOST(P0,P2cm,P2)
      RETURN
      END



      SUBROUTINE PHASE3(M0,M1,M2,M3,P0,P1,P2,P3,WGT)
      IMPLICIT NONE
      REAL*8 M0,M1,M2,M3,P0(0:3),P1(0:3),P2(0:3),P3(0:3),WGT
      REAL*8 pi,fact,XRN
      REAL*8 M12,E3,q3,phi3,sinth3,costh3
      REAL*8 P12cm(0:3),P3cm(0:3),P1cm(0:3),P2cm(0:3)
      PARAMETER (pi=3.1415926535,fact=2.533029d-2)     ! 2/(2Pi)/(4Pi)

      M12=(M1+M2)+XRN(0)*(M0-M1-M2-M3)        ! Generate uniformly m_12
      E3=M0/2. + (M3**2-M12**2)/(2.*M0)       ! E of particle 3 in CM frame
      q3=SQRT(E3**2-M3**2)
      phi3=2.*pi*XRN(0)                   ! Generate Omega uniformly
      costh3=2.*XRN(0)-1.
      sinth3=SQRT(1.-costh3**2)

      P3cm(0)=E3                          ! Calculate momenta in P0 CM frame
      P3cm(1)=q3*sinth3*COS(phi3)
      P3cm(2)=q3*sinth3*SIN(phi3)
      P3cm(3)=q3*costh3
      P12cm(0)=M0-E3
      P12cm(1)=-P3cm(1)
      P12cm(2)=-P3cm(2)
      P12cm(3)=-P3cm(3)

      CALL PHASE2(M12,M1,M2,P12cm,P1cm,P2cm,WGT)
      WGT=WGT*q3/M0*M12*(M0-M1-M2-M3)*fact

      CALL BOOST(P0,P1cm,P1)
      CALL BOOST(P0,P2cm,P2)
      CALL BOOST(P0,P3cm,P3)
      RETURN
      END


      SUBROUTINE PHASE3sym(M0,M1,M2,M3,P0,P1,P2,P3,WGT)
      IMPLICIT NONE
      REAL*8 M0,M1,M2,M3,P0(0:3),P1(0:3),P2(0:3),P3(0:3),WGT
      REAL*8 pi,fact,XRN,RAN2
      REAL*8 M12,E3,q3,phi3,sinth3,costh3
      REAL*8 P12cm(0:3),P3cm(0:3),P1cm(0:3),P2cm(0:3)
      INTEGER idum
      COMMON /ranno/ idum
      PARAMETER (pi=3.1415926535,fact=2.533029d-2)     ! 2/(2Pi)/(4Pi)

      M12=(M1+M2)+XRN(0)*(M0-M1-M2-M3)        ! Generate uniformly m_12
      E3=M0/2. + (M3**2-M12**2)/(2.*M0)       ! E of particle 3 in CM frame
      q3=SQRT(E3**2-M3**2)
      phi3=2.*pi*RAN2(idum)                   ! Generate Omega uniformly
      costh3=2.*XRN(0)-1.
      sinth3=SQRT(1.-costh3**2)

      P3cm(0)=E3                          ! Calculate momenta in P0 CM frame
      P3cm(1)=q3*sinth3*COS(phi3)
      P3cm(2)=q3*sinth3*SIN(phi3)
      P3cm(3)=q3*costh3
      P12cm(0)=M0-E3
      P12cm(1)=-P3cm(1)
      P12cm(2)=-P3cm(2)
      P12cm(3)=-P3cm(3)

      CALL PHASE2(M12,M1,M2,P12cm,P1cm,P2cm,WGT)
      WGT=WGT*q3/M0*M12*(M0-M1-M2-M3)*fact

      CALL BOOST(P0,P1cm,P1)
      CALL BOOST(P0,P2cm,P2)
      CALL BOOST(P0,P3cm,P3)
      RETURN
      END



      SUBROUTINE PHASE3tsym(M0,M1,M2,M3,P0,P1,P2,P3,WGT,IDIR)
      IMPLICIT NONE
      REAL*8 M0,M1,M2,M3,P0(0:3),P1(0:3),P2(0:3),P3(0:3),WGT
      REAL*8 pi,fact,XRN,RAN2
      REAL*8 M12,E3,q3,phi3,sinth3,costh3
      REAL*8 P12cm(0:3),P3cm(0:3),P1cm(0:3),P2cm(0:3)
      INTEGER IDIR
      INTEGER idum
      COMMON /ranno/ idum
      PARAMETER (pi=3.1415926535,fact=2.533029d-2)     ! 2/(2Pi)/(4Pi)
      REAL*8 a,nu
      PARAMETER (a=6462.5521d0,nu=0.95)
      REAL*8 t0,t1,t,rho,WTj

      M12=(M1+M2)+XRN(0)*(M0-M1-M2-M3)        ! Generate uniformly m_12
      E3=M0/2. + (M3**2-M12**2)/(2.*M0)       ! E of particle 3 in CM frame
      q3=SQRT(E3**2-M3**2)
      phi3=2.*pi*RAN2(idum)                   ! Generate Omega uniformly

      t0=-2d0*E3*M0
      t1=0d0
      rho=XRN(0)
      t=a-(rho*(a-t1)**(1d0-nu)+(1d0-rho)*(a-t0)**(1d0-nu))**
     &  (1d0/(1d0-nu))
      costh3=1d0+t/(E3*M0)
      IF (IDIR .EQ. 1) costh3=-costh3

      WTj = -((a-t1)**(1d0-nu)-(a-t0)**(1d0-nu))/(1d0-nu)
     &       *(a-t)**nu/(E3*M0)/2d0

      sinth3=SQRT(1.-costh3**2)

      P3cm(0)=E3                          ! Calculate momenta in P0 CM frame
      P3cm(1)=q3*sinth3*COS(phi3)
      P3cm(2)=q3*sinth3*SIN(phi3)
      P3cm(3)=q3*costh3
      P12cm(0)=M0-E3
      P12cm(1)=-P3cm(1)
      P12cm(2)=-P3cm(2)
      P12cm(3)=-P3cm(3)

!      CALL PHASE2(M12,M1,M2,P12cm,P1cm,P2cm,WGT)
      CALL PHASE2t(M12,M1,M2,P12cm,P1cm,P2cm,WGT,1-IDIR)
      WGT=WGT*q3/M0*M12*(M0-M1-M2-M3)*fact*WTj

      CALL BOOST(P0,P1cm,P1)
      CALL BOOST(P0,P2cm,P2)
      CALL BOOST(P0,P3cm,P3)
      RETURN
      END


      SUBROUTINE PHASE4(M0,M1,M2,M3,M4,P0,P1,P2,P3,P4,WGT)
      IMPLICIT NONE
      REAL*8 M0,M1,M2,M3,M4,P0(0:3),P1(0:3),P2(0:3),P3(0:3),P4(0:3),
     &  WGT
      REAL*8 pi,fact,XRN
      REAL*8 M123,E4,q4,phi4,sinth4,costh4
      REAL*8 P123cm(0:3),P4cm(0:3),P1cm(0:3),P2cm(0:3),P3cm(0:3)
      PARAMETER (pi=3.1415926535,fact=2.533029d-2)     ! 2/(2Pi)/(4Pi)

      M123=(M1+M2+M3)+XRN(0)*(M0-M1-M2-M3-M4)   ! Generate uniformly m_123
      E4=M0/2d0 + (M4**2-M123**2)/(2d0*M0)          ! E of 4 in CM frame
      q4=SQRT(E4**2-M4**2)
      phi4=2d0*pi*XRN(0)                       ! Generate Omega uniformly
      costh4=2d0*XRN(0)-1d0
      sinth4=SQRT(1d0-costh4**2)

      P4cm(0)=E4                          ! Calculate momenta in P0 CM frame
      P4cm(1)=q4*sinth4*COS(phi4)
      P4cm(2)=q4*sinth4*SIN(phi4)
      P4cm(3)=q4*costh4
      P123cm(0)=M0-E4
      P123cm(1)=-P4cm(1)
      P123cm(2)=-P4cm(2)
      P123cm(3)=-P4cm(3)

      CALL PHASE3(M123,M1,M2,M3,P123cm,P1cm,P2cm,P3cm,WGT)
      WGT=WGT*q4/M0*M123*(M0-M1-M2-M3-M4)*fact

      CALL BOOST(P0,P1cm,P1)
      CALL BOOST(P0,P2cm,P2)
      CALL BOOST(P0,P3cm,P3)
      CALL BOOST(P0,P4cm,P4)
      RETURN
      END


      SUBROUTINE PHASE4sym(M0,M1,M2,M3,M4,P0,P1,P2,P3,P4,WGT)
      IMPLICIT NONE
      REAL*8 M0,M1,M2,M3,M4,P0(0:3),P1(0:3),P2(0:3),P3(0:3),P4(0:3),
     &  WGT
      REAL*8 pi,fact,XRN,RAN2
      REAL*8 M123,E4,q4,phi4,sinth4,costh4
      REAL*8 P123cm(0:3),P4cm(0:3),P1cm(0:3),P2cm(0:3),P3cm(0:3)
      INTEGER idum
      COMMON /ranno/ idum
      PARAMETER (pi=3.1415926535,fact=2.533029d-2)     ! 2/(2Pi)/(4Pi)

      M123=(M1+M2+M3)+XRN(0)*(M0-M1-M2-M3-M4)   ! Generate uniformly m_123
      E4=M0/2d0 + (M4**2-M123**2)/(2d0*M0)          ! E of 4 in CM frame
      q4=SQRT(E4**2-M4**2)
      phi4=2d0*pi*RAN2(idum)                       ! Generate Omega uniformly
      costh4=2d0*XRN(0)-1d0
      sinth4=SQRT(1d0-costh4**2)

      P4cm(0)=E4                          ! Calculate momenta in P0 CM frame
      P4cm(1)=q4*sinth4*COS(phi4)
      P4cm(2)=q4*sinth4*SIN(phi4)
      P4cm(3)=q4*costh4
      P123cm(0)=M0-E4
      P123cm(1)=-P4cm(1)
      P123cm(2)=-P4cm(2)
      P123cm(3)=-P4cm(3)

      CALL PHASE3(M123,M1,M2,M3,P123cm,P1cm,P2cm,P3cm,WGT)
      WGT=WGT*q4/M0*M123*(M0-M1-M2-M3-M4)*fact

      CALL BOOST(P0,P1cm,P1)
      CALL BOOST(P0,P2cm,P2)
      CALL BOOST(P0,P3cm,P3)
      CALL BOOST(P0,P4cm,P4)
      RETURN
      END


      SUBROUTINE INITXRN(NDIM)
      IMPLICIT NONE
      INTEGER NDIM
      INTEGER MAXDIM
      PARAMETER (MAXDIM=50)
      REAL*8 RAN2
      REAL*8 RNDVAL(MAXDIM)
      INTEGER i,IPOS,IDUM
      COMMON /XRNDATA/ RNDVAL,IPOS
      COMMON /ranno/ idum

      DO i = 1,NDIM
        RNDVAL(i)=RAN2(idum)
c	print *,i,RNDVAL(i)
      ENDDO
      IPOS=1
      RETURN
      END

      
      SUBROUTINE STOREXRN(X,ND)
      IMPLICIT NONE
      INTEGER MAXDIM,ND,IPOS,I
      PARAMETER (MAXDIM=50)
      REAL*8 X(MAXDIM),RNDVAL(MAXDIM)
      COMMON /XRNDATA/ RNDVAL,IPOS
      
      DO i=1,ND
        IF ((X(i) .GE. 0d0) .AND. (X(i) .LE. 1d0)) THEN 
          RNDVAL(i)=X(i)
c	  print *,x(i)
        ELSE
          PRINT *,'Wrong range in rndval'
	  print *,i,x(i)
          STOP
        ENDIF
      ENDDO
      IPOS=1
      RETURN
      END


      REAL*8 FUNCTION XRN(idum)
      IMPLICIT REAL*8 (A-H,O-Z)
      PARAMETER (MAXDIM=50)
      DIMENSION RNDVAL(MAXDIM)
      COMMON /XRNDATA/ RNDVAL,IPOS
      XRN=RNDVAL(IPOS)
c      print *,ipos,xrn
      IPOS=IPOS+1
      RETURN
      END

