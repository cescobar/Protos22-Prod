c      GWF(1)=-g/2d0
c      GWF(2)=-g/2d0

      GWF(1)=-g/SQRT(2d0)
      GWF(2)=0d0

!     two factor to keep standard colour factors for lambda/2 in QCD vertex 
!     (anomalous operator has lambda)

      G2Gtq(1)=-2d0*gs/mt*zL     ! t in q out: top decay / tbar production
      G2Gtq(2)=-2d0*gs/mt*zR
      G2Gqt(1)=-G2Gtq(2)         ! t out q in: antitop decay / t production
      G2Gqt(2)=-G2Gtq(1)

