!     W coupling to SM fermions

      GWF(1)=-g/SQRT(2d0)
      GWF(2)=0d0

!     Z couplings to SM fermions

      GZuu(1)=-gcw*(1d0/2d0-sw2*2d0/3d0)
      GZuu(2)=gcwsw2*2d0/3d0

      GZdd(1)=-gcw*(-1d0/2d0+sw2/3d0)
      GZdd(2)=-gcwsw2/3d0

      GZvv(1)=-gcw*(1d0/2d0)
      GZvv(2)=0d0

      GZll(1)=-gcw*(-1d0/2d0+sw2)
      GZll(2)=-gcwsw2

!     Doublet-doublet-gauge boson

      GWEN(1)=GWF(1)            ! WEN
      GWEN(2)=GWF(1)

      GZEE(1)=GZll(1)          ! ZEE
      GZEE(2)=GZll(1)

      GAEE(1)=e             ! AEE
      GAEE(2)=e

      GZNN(1)=GZvv(1)
      GZNN(2)=GZvv(1)

!     E-light fermion-gauge boson/Higgs
!     Normalised to Vln = 1

      GWEv(1)=0d0                      ! WEv
      GWEv(2)=0d0

      GZEl(1)=0d0                      ! ZEl
      GZEl(2)=-gcw/2d0

      GHEl(1)=g/2d0*(mL/MW)           ! HEl, E in l out
      GHEl(2)=0d0

      GHlE(1)=GHEl(2)                  ! HEl, E out l in (actually, conjg)
      GHlE(2)=GHEl(1)

!     N-light fermion-gauge boson and Higgs
!     Normalised to Vln = 1

      GWlN(1)=0d0                      ! WlN
      GWlN(2)=-GWF(1)

      GZNv(1)=0d0                      ! ZNv, N in v out
      GZnv(2)=-GZNv(1)
      
      GZvN(1)=GZNv(1)                  ! N out v in is the complex conjugate
      GZvN(2)=GZNv(2)
      
      GHNv(2)=0d0                      ! HNv, N in v out
      GHNv(1)=GHNv(2)
      
      GHvN(1)=GHNv(2)                  ! N out v in (actually, conjg)
      GHvN(2)=GHNv(1)

