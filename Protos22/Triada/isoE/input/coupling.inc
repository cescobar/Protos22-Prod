!     W coupling to SM fermions

      GWF(1)=-g/SQRT(2d0)
      GWF(2)=0d0

!     Z couplings to SM fermions

      GZuu(1)=-gcw*(1d0/2d0-sw2*2d0/3d0)
      GZuu(2)=gcwsw2*2d0/3d0

      GZdd(1)=-gcw*(-1d0/2d0+sw2/3d0)
      GZdd(2)=-gcwsw2/3d0

      GZvv(1)=-gcw*(1d0/2d0)
      GZvv(2)=0d0

      GZll(1)=-gcw*(-1d0/2d0+sw2)
      GZll(2)=-gcwsw2

!     singlet-singlet-gauge boson

      GZEE(1)=GZll(2)          ! ZEE
      GZEE(2)=GZll(2)

      GAEE(1)=e             ! AEE
      GAEE(2)=e

!     E-light fermion-gauge boson/Higgs
!     Normalised to Vln = 1

      GWEv(1)=GWF(1)                      ! WEv
      GWEv(2)=0d0

      GZEl(1)=gcw/2d0                     ! ZEl
      GZEl(2)=0d0

      GHEl(1)=0d0                      ! HEl, E in l out
      GHEl(2)=g/2d0*(mE/MW)

      GHlE(1)=GHEl(2)                  ! HEl, E out l in (actually, conjg)
      GHlE(2)=GHEl(1)

      

