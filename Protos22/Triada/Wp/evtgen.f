      REAL*8 FUNCTION FXN(X,WGT)
      IMPLICIT NONE

!     Parameters

      INCLUDE 'input/generator.inc'

!     Arguments

      REAL*8 X(MAXDIM),WGT

!     Data needed

      INTEGER IRECORD,IWRITE,IHISTO
      COMMON /FXNflags/ IRECORD,IWRITE,IHISTO
      REAL*8 fact(NPROC),WT_ITMX
      COMMON /GLOBALFACT/ fact,WT_ITMX

!     Multigrid integration: process selection done by VEGAS

      INTEGER INITGRID,IPROC
      COMMON /multigr/ INITGRID,IPROC

!     Output

      REAL*8 FXNi(NPROC)
      COMMON /CROSS/ FXNi
      REAL*8 SIG(MAXGR),SIG2(MAXGR),ERR(MAXGR)
      INTEGER NIN,NOUT
      COMMON /STATS/ SIG,SIG2,ERR,NIN,NOUT

!     Local variables      

      REAL*8 WTPS,WTME
      INTEGER i,INOT
      INTEGER ND

!     --------------------------------------------

      ND=NDIM
      CALL STOREXRN(X,ND)

      FXN=0d0
      IF (IRECORD .EQ. 1) NIN=NIN+1

      DO i=1,NPROC
        FXNi(i)=0d0
      ENDDO

      IF (IPROC .GT. NPROC) THEN
        PRINT 1301,IPROC
        RETURN
      ENDIF

      CALL GENMOM(WTPS)
      IF (WTPS .EQ. 0d0) RETURN

      CALL PSCUT(INOT)
      IF (INOT .EQ. 1) RETURN

      CALL MSQ(WTME)

      IF (IRECORD .EQ. 1)  NOUT=NOUT+1
      FXNi(IPROC)=WTME*WTPS*fact(IPROC)*WGT
      FXN=FXNi(IPROC)

      IF (INITGRID .EQ. 0) THEN
        SIG(IPROC)=SIG(IPROC)+FXNi(IPROC)*WT_ITMX
        SIG2(IPROC)=SIG2(IPROC)+FXNi(IPROC)**2*WT_ITMX
        ERR(IPROC)=SQRT(SIG2(IPROC)-SIG(IPROC)**2/FLOAT(NIN))*WT_ITMX
      ENDIF

      IF (IHISTO .EQ. 1) CALL ADDEV(FXN*WT_ITMX)
      IF (IWRITE .EQ. 1) CALL EVTOUT(0)
      FXN=FXN/WGT
      RETURN
1301  FORMAT ('Warning: wrong IPROC = ',I2,' found, skipping...')
      END



      SUBROUTINE GENMOM(WTPS)
      IMPLICIT NONE

!     Arguments

      REAL*8 WTPS

!     Multigrid integration: process selection done by VEGAS

      INTEGER INITGRID,IPROC
      COMMON /multigr/ INITGRID,IPROC

!     Data needed

      REAL*8 MZ,GZ,MW,GW,mt,Gt,mb,MH,GH,mtau,mc
      COMMON /SMMASS/ MZ,GZ,MW,GW,mt,Gt,MH,GH,mb,mc,mtau
      REAL*8 mN,GN
      COMMON /Nmass/ mN,GN
      REAL*8 MWP,GWP
      COMMON /WPmass/ MWP,GWP
      REAL*8 NGN,NGW,NGZ,NGt
      COMMON /BWdata/ NGN,NGW,NGZ,NGt
      REAL*8 pi,ET
      COMMON /const/ pi,ET
      REAL*8 WTT1(5),WTFL(3),WTFR(3),WTZ(4)
      REAL*8 BRT1ac(0:5),BRFLac(0:3),BRFRac(0:3),BRZac(0:4)
      COMMON /DECCH/ WTT1,WTFL,WTFR,WTZ,BRT1ac,BRFLac,BRFRac,BRZac
      INTEGER IDW1,IDZ1,IDH1,IDjj1,IDtb1,IDLNC,IDLNV
      COMMON /Nfstate/ IDW1,IDZ1,IDH1,IDjj1,IDtb1,IDLNC,IDLNV

!     External functions used

      REAL*8 XRN,RAN2
      INTEGER idum
      COMMON /ranno/ idum

!     Outputs

      INTEGER IMA,IQ,IL1,IL2,IB1,IF1,ILNV
      COMMON /Nflags/ IMA,IQ,IL1,IL2,IB1,IF1,ILNV
      REAL*8 Q1(0:3),Q2(0:3)
      COMMON /MOMINI/ Q1,Q2
      REAL*8 PN(0:3),PB1(0:3),Pt(0:3),PW(0:3)
      COMMON /MOMINT/ PN,PB1,Pt,PW
      REAL*8 Pl1(0:3),Pl2(0:3),Pf1(0:3),Pfb1(0:3)
      COMMON /MOMEXT/ Pl1,Pl2,Pf1,Pfb1
      REAL*8 Pf2(0:3),Pfb2(0:3)
      COMMON /MOMEXTRA/ Pf2,Pfb2
      REAL*8 x1,x2,s,Q
      INTEGER IDIR
      COMMON /miscdata/ x1,x2,s,Q,IDIR
      REAL*8 countB(5),countP(3),countL(3),countR(3)
      COMMON /testdec/ countB,countP,countL,countR

!     Local variables

      REAL*8 r,y1,y2,flux
      REAL*8 Qsq,Qmin,Qmax,y,ymin,ymax
      REAL*8 PCM_LAB(0:3)
      REAL*8 QN,QB1,Qt,QW,m1,m2
      REAL*8 MB1,GB1,NGB1
      REAL*8 WT1,WT2,WT_BREIT,WT_PDF,WT_PROD,WT_DEC,WTFS
      REAL*8 RCH,ICH

!     --------------------------------------------

      WTPS=0d0
      

      IDIR=0
      IF (IPROC .GT. 4) IDIR=1
      IF ((IPROC .EQ. 1) .OR. (IPROC .EQ. 3) .OR. (IPROC .EQ. 5)
     &   .OR. (IPROC .EQ. 7)) THEN
        IQ=1        !  u d~   c s~
      ELSE
        IQ=2        !  d u~   s c~
      ENDIF

!     Select final state

      WTFS=1d0

!     Boson from T1

      RCH=RAN2(idum)
      ICH=0
      DO WHILE (BRT1ac(ICH) .LT. RCH)
        ICH=ICH+1
      ENDDO
      IB1=ICH

      WTFS=WTFS*WTT1(IB1)

!     Lepton L2 produced in W decay

      RCH=RAN2(idum)
      ICH=0
      DO WHILE (BRFRac(ICH) .LT. RCH)
        ICH=ICH+1
      ENDDO
      IL2=ICH
      WTFS=WTFS*WTFR(IL2)

!     Lepton L1 produced in N decay

      RCH=RAN2(idum)
      ICH=0
      IF (IB1 .LE. 3) THEN
        DO WHILE (BRFLac(ICH) .LT. RCH)
          ICH=ICH+1
        ENDDO
        IL1=ICH
        WTFS=WTFS*WTFL(IL1)
      ELSE
        DO WHILE (BRFRac(ICH) .LT. RCH)
          ICH=ICH+1
        ENDDO
        IL1=ICH
        WTFS=WTFS*WTFR(IL1)
      ENDIF

!     Fermions from B1 decay (for W, Z and W')

      IF (IB1 .EQ. 1) THEN
        IF1=1
        WTFS=WTFS*9d0
      ELSE IF (IB1 .EQ. 2) THEN
        RCH=RAN2(idum)
        ICH=0
        DO WHILE (BRZac(ICH) .LT. RCH)
          ICH=ICH+1
        ENDDO
        IF1=ICH-1
        WTFS=WTFS*WTZ(ICH)
      ELSE IF (IB1 .EQ. 4) THEN
        IF1=1
        WTFS=WTFS*6d0
      ELSE IF (IB1 .EQ. 5) THEN
        IF1=1
        WTFS=WTFS*3d0*9d0
      ENDIF

!     Select LNC, LNV

      ILNV=0
      IF (IMA .EQ. 0) GOTO 5
      IF ((IB1 .EQ. 1) .OR. (IB1 .EQ. 4) .OR. (IB1 .EQ. 5)) THEN
        IF ((IDLNC .EQ. 1) .AND. (IDLNV .EQ. 1)) THEN
          RCH=RAN2(idum)
          IF (RCH .GT. 0.5d0) ILNV=1
          WTFS=WTFS*2d0
        ELSE IF (IDLNV .EQ. 1) THEN
          ILNV=1
        ELSE IF ((IDLNC .EQ. 0) .AND. (IDLNV .EQ. 0)) THEN
          PRINT *,'ERROR: IDLNC = IDLNV = 0 and IB = 1'
          STOP
        ENDIF
      ENDIF

5     countB(IB1)=countB(IB1)+1d0
      countP(IL2)=countP(IL2)+1d0
      IF (IB1 .LE. 3) THEN
        countL(IL1)=countL(IL1)+1d0
      ELSE
        countR(IL1)=countR(IL1)+1d0
      ENDIF

!     Select masses of final state

      IF (IB1 .EQ. 1) THEN
        MB1=MW
        GB1=GW
        NGB1=NGW
      ELSE IF (IB1 .EQ. 2) THEN
        MB1=MZ
        GB1=GZ
        NGB1=NGZ
      ELSE IF (IB1 .EQ. 3) THEN
        QB1=MH
      ELSE IF (IB1 .EQ. 4) THEN
        QB1=0d0
      ELSE IF (IB1 .EQ. 5) THEN
        QB1=0d0
      ELSE
        PRINT *,'IB1 = ',IB1,' in genmom'
        STOP
      ENDIF

!     Generation of the masses of the virtual particles

      CALL BREIT(mN,GN,NGN,QN,WT1)
      WT_BREIT=WT1*(2d0*pi)**3
      IF ((IB1 .EQ. 1) .OR. (IB1 .EQ. 2)) THEN
        CALL BREIT(MB1,GB1,NGB1,QB1,WT2)
        WT_BREIT=WT_BREIT*WT2*(2d0*pi)**3
      ELSE IF (IB1 .EQ. 5) THEN
        CALL BREIT(MW,GW,NGW,QW,WT2)
        WT_BREIT=WT_BREIT*WT2*(2d0*pi)**3
        CALL BREIT(mt,Gt,NGt,Qt,WT2)
        WT_BREIT=WT_BREIT*WT2*(2d0*pi)**3
        IF (Qt .GT. QN) RETURN
        IF (QW .GT. Qt) RETURN
      ENDIF
      IF (QB1 .GT. QN) RETURN

c      print *,il1,il2
c      print *,ib1,ib2
c      print *,QT1,QT2,qb1,qb2

!     Generation of the momentum fractions x1, x2

      y1=XRN(0)
      y2=XRN(0)

      Qmin=0d0
      Qmax=2d0*MWP
      ymin=(atan((Qmin**2-MWP**2)/(MWP*GWP))/(MWP*GWP))
      ymax=(atan((Qmax**2-MWP**2)/(MWP*GWP))/(MWP*GWP))
      y=ymin+(ymax-ymin)*y2
      Qsq=sqrt(MWP**2+MWP*GWP*tan(MWP*GWP*y))
      r=(Qsq)**2/ET**2
      x1=r**(y1)                                                 
      x2=r**(1d0-y1)
      WT_PDF=ABS(LOG(r))/ET**2*(ymax-ymin)*
     &  ((Qsq**2-MWP**2)**2+(MWP*GWP)**2)
      
      s=x1*x2*ET**2
      flux=2d0*s
      IF (SQRT(s) .LT. QN) RETURN

!     Initial parton momenta in LAB system

      m1=0d0
      m2=0d0
      
      Q1(3)=ET*x1/2d0
      Q1(1)=0d0
      Q1(2)=0d0
      Q1(0)=SQRT(Q1(3)**2+m1**2)
      IF (Q1(0) .GE. ET/2d0) RETURN

      Q2(3)=-ET*x2/2d0
      Q2(1)=0d0
      Q2(2)=0d0
      Q2(0)=SQRT(Q2(3)**2+m2**2)
      IF (Q2(0) .GE. ET/2d0) RETURN

      PCM_LAB(0)=Q1(0)+Q2(0)
      PCM_LAB(1)=0d0
      PCM_LAB(2)=0d0
      PCM_LAB(3)=Q1(3)+Q2(3)

!     Generation

      CALL PHASE2sym(SQRT(s),0d0,QN,PCM_LAB,Pl2,PN,WT1)  ! l2 = l from W
      WT_PROD=(2d0*pi)**4*WT1

c      print *,PT1,SQRT(DOT(PT1,PT1))
c      print *,PT2,SQRT(DOT(PT2,PT2))

      IF (IB1 .LE. 3) THEN

      CALL PHASE2(QN,0d0,QB1,PN,Pl1,PB1,WT1)

!     B1 decay, if W,Z

      WT2=1d0
      IF (IB1 .NE. 3) CALL PHASE2(QB1,0d0,0d0,PB1,Pf1,Pfb1,WT2)
      WT_DEC=WT1*WT2
      
      ELSE IF (IB1 .EQ. 4) THEN
      
      CALL PHASE3(QN,0d0,0d0,0d0,PN,Pl1,Pf1,Pfb1,WT1)
      WT_DEC=WT1

      ELSE

      CALL PHASE3(QN,0d0,Qt,0d0,PN,Pl1,Pt,Pfb1,WT1)      ! Assume decay to t bbar
      WT_DEC=WT1
      
      CALL PHASE2(Qt,QW,0d0,Pt,PW,Pf1,WT1)
      WT_DEC=WT_DEC*WT1
      CALL PHASE2(QW,0d0,0d0,PW,Pf2,Pfb2,WT1)
      WT_DEC=WT_DEC*WT1

      IF (     ((IQ .EQ. 1) .AND. (ILNV .EQ. 1))         ! It is tbar b
     &    .OR. ((IQ .EQ. 2) .AND. (ILNV .EQ. 0)) )
     &   CALL EXCHANGE(Pf1,Pfb1)

      ENDIF

      WTPS=WT_BREIT*WT_PROD*WT_DEC*WT_PDF*WTFS/flux
      
c      print *,'WT = ',WTPS
c      STOP
      RETURN
      END




      SUBROUTINE PSCUT(INOT)
      IMPLICIT NONE

!     Arguments

      INTEGER INOT

!     Data needed

      REAL*8 Q1(0:3),Q2(0:3)
      COMMON /MOMINI/ Q1,Q2
      REAL*8 PN(0:3),PB1(0:3),Pt(0:3),PW(0:3)
      COMMON /MOMINT/ PN,PB1,Pt,PW
      REAL*8 Pl1(0:3),Pl2(0:3),Pf1(0:3),Pfb1(0:3)
      COMMON /MOMEXT/ Pl1,Pl2,Pf1,Pfb1
      INTEGER ICUT
      COMMON /CUTFLAGS/ ICUT

!     External functions used

c      REAL*8 RLEGO,RAP

!     Local

!     --------------------------------------------

      INOT=1

98    INOT=0

      RETURN
      END




      SUBROUTINE MSQ(WTME)
      IMPLICIT NONE

!     Parameters

      INCLUDE 'input/generator.inc'

!     External functions

      REAL*8 QQ_lN

!     Arguments (= output)

      REAL*8 WTME

!     Multigrid integration

      INTEGER INITGRID,IPROC
      COMMON /multigr/ INITGRID,IPROC

!     Data needed

      REAL*8 MZ,GZ,MW,GW,mt,Gt,mb,MH,GH,mtau,mc
      COMMON /SMMASS/ MZ,GZ,MW,GW,mt,Gt,MH,GH,mb,mc,mtau
      REAL*8 MWP,GWP
      COMMON /WPmass/ MWP,GWP
      REAL*8 Q1(0:3),Q2(0:3)
      COMMON /MOMINI/ Q1,Q2
      REAL*8 PN(0:3),PB1(0:3),Pt(0:3),PW(0:3)
      COMMON /MOMINT/ PN,PB1,Pt,PW
      REAL*8 Pl1(0:3),Pl2(0:3),Pf1(0:3),Pfb1(0:3)
      COMMON /MOMEXT/ Pl1,Pl2,Pf1,Pfb1
      INTEGER IMA,IQ,IL1,IL2,IB1,IF1,ILNV
      COMMON /Nflags/ IMA,IQ,IL1,IL2,IB1,IF1,ILNV
      INTEGER nhel8(NPART,3**NPART),nhel6(NPART,3**NPART),
     &        nhel4(NPART,3**NPART)
      LOGICAL GOODHEL(MAXAMP,3**NPART)
      INTEGER ncomb8,ncomb6,ncomb4,ntry(MAXAMP)
      COMMON /HELI/ nhel8,nhel6,nhel4,GOODHEL,ncomb8,ncomb6,ncomb4,ntry

      REAL*8 x1,x2,s,Q
      INTEGER IDIR
      COMMON /miscdata/ x1,x2,s,Q,IDIR
      INTEGER IPDSET,IPPBAR
      COMMON /PDFFLAGS/ IPDSET,IPPBAR
      REAL*8 QFAC
      COMMON /PDFSCALE/ QFAC

!     Local variables

      REAL*8 fu1,fd1,fus1,fds1,fs1,fc1,fb1,fg1
      REAL*8 fu2,fd2,fus2,fds2,fs2,fc2,fb2,fg2
      INTEGER i,k,ISTAT,IAMP,ncomb
      REAL*8 M1,ME,STR(NPROC)

!     --------------------------------------------

      WTME=0d0

!     Scale for structure functions

      Q=SQRT(s)
     
      CALL GETPDF(x1,Q,fu1,fd1,fus1,fds1,fs1,fc1,fb1,fg1,ISTAT)
      IF (ISTAT .LT. 0) RETURN
      CALL GETPDF(x2,Q,fu2,fd2,fus2,fds2,fs2,fc2,fb2,fg2,ISTAT)
      IF (ISTAT .LT. 0) RETURN

c      IF (IP .EQ. 1) THEN
c        IF (IPPBAR .EQ. 0) THEN
c          STR(1)=fu1*fus2         ! u  u~ 
c          STR(2)=fd1*fds2         ! d  d~
c          STR(5)=fus1*fu2         ! u~ u
c          STR(6)=fds1*fd2         ! d~ d
c        ELSE
c          STR(1)=fu1*fu2          ! u  u~ 
c          STR(2)=fd1*fd2          ! d  d~
c          STR(5)=fus1*fus2        ! u~ u
c          STR(6)=fds1*fds2        ! d~ d
c        ENDIF
c        STR(3)=fc1*fc2            ! c  c~
c        STR(4)=fs1*fs2            ! s  s~
c        STR(7)=fc1*fc2            ! c~ c
c        STR(8)=fs1*fs2            ! s~ s
c      ELSE
        IF (IPPBAR .EQ. 0) THEN
          STR(1)=fu1*fds2         ! u  d~ 
          STR(2)=fd1*fus2         ! d  u~ 
          STR(5)=fds1*fu2         ! d~ u 
          STR(6)=fus1*fd2         ! u~ d   
        ELSE
          STR(1)=fu1*fd2          ! u  d~ 
          STR(2)=fd1*fu2          ! d  u~ 
          STR(5)=fds1*fus2        ! d~ u 
          STR(6)=fus1*fds2        ! u~ d   
        ENDIF
        STR(3)=fc1*fs2            ! c  s~ 
        STR(4)=fs1*fc2            ! s  c~ 
        STR(7)=fs1*fc2            ! s~ c 
        STR(8)=fc1*fs2            ! c~ s   
c      ENDIF

!     Select helicity amplitude and process

      i=IB1
      IAMP=i+5*(IPROC-1)
      ntry(IAMP)=ntry(IAMP)+1
      IF (ntry(IAMP) .GT. 200) ntry(IAMP)=200

c      PRINT *,'IAMP = ',IAMP,' ntry = ',ntry(IAMP)

      IF (IB1 .EQ. 3) THEN
        ncomb=ncomb4
      ELSE IF (IB1 .EQ. 5) THEN
        ncomb=ncomb8
      ELSE
        ncomb=ncomb6
      ENDIF

c      print *,'ncomb = ',ncomb

      IF (IDIR .EQ. 1) CALL EXCHANGE(Q1,Q2)

      ME=0d0
      DO k=1,ncomb
        IF (GOODHEL(IAMP,k) .OR. ntry(IAMP) .LT. 100) THEN
          IF (IB1 .EQ. 3) THEN
            M1=QQ_lN(nhel4(1,k))
          ELSE IF (IB1 .EQ. 5) THEN
            M1=QQ_lN(nhel8(1,k))
          ELSE
            M1=QQ_lN(nhel6(1,k))
          ENDIF
          ME=ME+M1
          IF(M1 .GT. 0d0 .AND. .NOT. GOODHEL(IAMP,k)) THEN
            GOODHEL(IAMP,k)=.TRUE.
          ENDIF
        ENDIF
      ENDDO
      ME=ME/4d0

20    IF (IDIR .EQ. 1) CALL EXCHANGE(Q1,Q2)

      WTME=ME*STR(IPROC)

      RETURN
      END

