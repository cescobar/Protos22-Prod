      SUBROUTINE PLOTINI(IMODE)
      IMPLICIT NONE

!     Parameters

      INCLUDE 'input/output.inc'

!     Arguments

      INTEGER IMODE

!     For file output

      CHARACTER*100 PROCNAME
      INTEGER l
      COMMON /prname/ PROCNAME,l
      INTEGER NRUNS,IRUN
      COMMON /runs/ NRUNS,IRUN

!     Output

      REAL*8 varmin(nvar),varmax(nvar)
      INTEGER nbintot(nvar)
      COMMON /PLOTRNG/ varmin,varmax,nbintot
      REAL*8 varsig(nvar,maxbins)
      COMMON /PLOTDATA/ varsig

!     Local variables

      INTEGER j,k
      REAL*8 bin,value
!                   MQ1  MQ2  MB1  MB2  MW1  MW1b  MW2 MW2b  MQQ
      DATA varmin  /250.,250.,  0.,  0.,  0.,  0.,  0.,  0.,   0.,-1.,
     & -1./
      DATA varmax  /750.,750.,200.,200.,200.,750.,200.,750.,4000., 1.,
     &  1./
      DATA nbintot /100,  100, 100, 100, 100, 150, 100, 150,  200, 40,
     & 40/

!     Initialise

      IF (IMODE .NE. -1) GOTO 11

      DO k=1,nvar
        DO j=1,maxbins
          varsig(k,j)=0d0
        ENDDO
      ENDDO
      
      
      RETURN

11    IF (IMODE .NE. 1) GOTO 12

      DO k=1,nvar
        IF (NRUNS .EQ. 1) THEN
          OPEN (66,file='plots/'//procname(1:l)//'-'
     &      //char(48+k/10)//char(48+MOD(k,10)))
        ELSE
          OPEN (66,file='plots/'//procname(1:l)//'-r'
     &      //char(48+IRUN/10)//char(48+MOD(IRUN,10))//'-'
     &      //char(48+k/10)//char(48+MOD(k,10)))
        ENDIF
        DO j=1,nbintot(k)
          bin=varmin(k)+(FLOAT(j)-1d0)*
     &    (varmax(k)-varmin(k))/float(nbintot(k))
          value=varsig(k,j)
          WRITE (66,1000) bin,value
        ENDDO
        CLOSE (66)
      ENDDO
      RETURN

12    PRINT *,'Wrong IMODE'
      STOP
1000  FORMAT (' ',D10.4,' ',D10.4)
      END




      SUBROUTINE ASINI(IMODE)
      IMPLICIT NONE

!     Parameters

      INCLUDE 'input/generator.inc'
      INCLUDE 'input/output.inc'

!     Arguments

      INTEGER IMODE

!     Asymmetry information data

      REAL*8 asym_cent(n_a)
      COMMON /ASYMRNG/ asym_cent
      REAL*8 SIGp(n_a),SIGm(n_a),SIG2p(n_a),SIG2m(n_a)
      COMMON /ASYMDATA/ SIGp,SIGm,SIG2p,SIG2m

!     For statistics

      REAL*8 SIG(MAXGR),SIG2(MAXGR),ERR(MAXGR)
      INTEGER NIN,NOUT
      COMMON /STATS/ SIG,SIG2,ERR,NIN,NOUT

!     For logging

      REAL*8 asym(n_a)
      COMMON /ASYMRES/ asym

!     Local to save

      LOGICAL GOODASYM(n_a)
      SAVE GOODASYM

!     Local

      INTEGER i
      REAL*8 ERRp(n_a),ERRm(n_a),err_asym(n_a)
      REAL*8 beta,Ap,Am,FL,F0,FR

      DATA asym_cent /0.,-0.5874,0.5874,0.,-0.5874,0.5874,
     &  0.,-0.5874,0.5874,0.,-0.5874,0.5874/

!     Initialise

      IF (IMODE .NE. -1) GOTO 11

      DO i=1,n_a
        SIGp(i)=0d0
        SIGm(i)=0d0
        SIG2p(i)=0d0
        SIG2m(i)=0d0
      ENDDO

      RETURN

11    IF (IMODE .NE. 1) GOTO 12

      DO i=1,n_a
        IF (SIGp(i)+SIGm(i) .GT. 0) THEN
        GOODASYM(i)=.TRUE.
          ERRp(i)=SQRT(SIG2p(i)-SIGp(i)**2/FLOAT(NIN))
          ERRm(i)=SQRT(SIG2m(i)-SIGm(i)**2/FLOAT(NIN))
          asym(i)=(SIGp(i)-SIGm(i))/(SIGp(i)+SIGm(i))
          err_asym(i)=2d0/(SIGp(i)+SIGm(i))**2*
     .      SQRT((SIGm(i)*ERRp(i))**2+(SIGp(i)*ERRm(i))**2)
        ELSE
          GOODASYM(i)=.FALSE.
          asym(i)=0d0
        ENDIF      
      ENDDO
      RETURN

12    IF (IMODE .NE. 2) GOTO 13

      beta=2**(1d0/3d0)-1d0

      IF (GOODASYM(1)) THEN
      PRINT 1150,asym(1),err_asym(1)
      PRINT 1151,asym(2),err_asym(2)
      PRINT 1152,asym(3),err_asym(3)
      Ap=asym(2)
      Am=asym(3)
      FR=1d0/(1d0-beta)+(Am-beta*Ap)/(3d0*beta*(1d0-beta**2))
      FL=1d0/(1d0-beta)-(Ap-beta*Am)/(3d0*beta*(1d0-beta**2))
      F0=-(1d0+beta)/(1d0-beta)+(Ap-Am)/(3d0*beta*(1d0-beta))
      PRINT 1200,FL,F0,FR
      ENDIF

      IF (GOODASYM(4)) THEN
      PRINT 1160,asym(4),err_asym(4)
      PRINT 1161,asym(5),err_asym(5)
      PRINT 1162,asym(6),err_asym(6)
      Ap=asym(5)
      Am=asym(6)
      FR=1d0/(1d0-beta)+(Am-beta*Ap)/(3d0*beta*(1d0-beta**2))
      FL=1d0/(1d0-beta)-(Ap-beta*Am)/(3d0*beta*(1d0-beta**2))
      F0=-(1d0+beta)/(1d0-beta)+(Ap-Am)/(3d0*beta*(1d0-beta))
      PRINT 1200,FL,F0,FR
      ENDIF
      PRINT 999
      RETURN

13    PRINT *,'Wrong IMODE'
      STOP
999   FORMAT ('')
1150  FORMAT ('AFB t  ',F8.5,' +- ',F8.5,' (MC)')
1151  FORMAT ('A+  t  ',F8.5,' +- ',F8.5,' (MC)')
1152  FORMAT ('A-  t  ',F8.5,' +- ',F8.5,' (MC)')
1160  FORMAT ('AFB tb ',F8.5,' +- ',F8.5,' (MC)')
1161  FORMAT ('A+  tb ',F8.5,' +- ',F8.5,' (MC)')
1162  FORMAT ('A-  tb ',F8.5,' +- ',F8.5,' (MC)')
1200  FORMAT ('--->  FL ~ ',F8.5,'   F0 ~ ',F8.5,'   FR ~ ',F8.5)
      END





      SUBROUTINE ADDEV(WT)
      IMPLICIT NONE

!     Parameters

      INCLUDE 'input/output.inc'

!     Arguments

      REAL*8 WT

!     External functions used

      REAL*8 DOT,COSVEC

!     External momenta

      REAL*8 Q1(0:3),Q2(0:3)
      COMMON /MOMINI/ Q1,Q2
      REAL*8 PQ1(0:3),PQ2(0:3),Pt1(0:3),Pt2(0:3),PW1(0:3),PW2(0:3)
      COMMON /MOMINT/ PQ1,PQ2,Pt1,Pt2,PW1,PW2
      REAL*8 PB1(0:3),PB2(0:3)
      COMMON /MOMINT2/ PB1,PB2
      REAL*8 Pb(0:3),Pbb(0:3),Pf1(0:3),Pfb1(0:3),Pf2(0:3),Pfb2(0:3),
     &  Pf3(0:3),Pfb3(0:3),Pf4(0:3),Pfb4(0:3)
      COMMON /MOMEXT/ Pb,Pbb,Pf1,Pfb1,Pf2,Pfb2,Pf3,Pfb3,Pf4,Pfb4

      REAL*8 x1,x2,s,Q
      INTEGER IDIR
      COMMON /miscdata/ x1,x2,s,Q,IDIR
      INTEGER IMOD,IB1,IB2,IZF1,IZF2
      COMMON /Qflags/ IMOD,IB1,IB2,IZF1,IZF2

!     Range of fariables

      REAL*8 varmin(nvar),varmax(nvar)
      INTEGER nbintot(nvar)
      COMMON /PLOTRNG/ varmin,varmax,nbintot
      REAL*8 varsig(nvar,maxbins)
      COMMON /PLOTDATA/ varsig

!     Asymmetry information data

      REAL*8 asym_cent(n_a)
      COMMON /ASYMRNG/ asym_cent
      REAL*8 SIGp(n_a),SIGm(n_a),SIG2p(n_a),SIG2m(n_a)
      COMMON /ASYMDATA/ SIGp,SIGm,SIG2p,SIG2m

!     Local kinematical quantities

      REAL*8 Pall(0:3),Pboo(0:3),Ptrec(0:3),Ptrec2(0:3)
      REAL*8 cos_lW1,cos_lW2,cos_l1,cos_l2,cos_lZ1,cos_lZ2


!     Dummy variables

      INTEGER i,nbin
      REAL*8 var(nvar),val,quant(n_a)
      REAL*8 pdum1(0:3),pdum2(0:3)

      DO i=1,n_a
        quant(i)=asym_cent(i)
      ENDDO
      DO i=1,nvar
        var(i)=varmin(i)
      ENDDO

      var(1)=SQRT(DOT(PQ1,PQ1))
      var(2)=SQRT(DOT(PQ2,PQ2))
      var(3)=SQRT(DOT(PB1,PB1))
      var(4)=SQRT(DOT(PB2,PB2))

      CALL SUMVEC(Pf3,Pfb3,Pdum1)
      var(5)=SQRT(DOT(Pdum1,Pdum1))            ! W+ top
      CALL SUMVEC(Pdum1,Pb,Ptrec)
      var(6)=SQRT(DOT(Ptrec,Ptrec))            ! top
      CALL SUMVEC(Pf4,Pfb4,Pdum1)
      var(7)=SQRT(DOT(Pdum1,Pdum1))            ! W- antitop
      CALL SUMVEC(Pdum1,Pbb,Ptrec2)
      var(8)=SQRT(DOT(Ptrec2,Ptrec2))            ! antitop
      CALL SUMVEC(PQ1,PQ2,Pall)
      var(9)=SQRT(DOT(Pall,Pall))

!     lW distribution for top

      IF (IB1 .EQ. 1) THEN
      Pboo(0)=PW1(0)                                ! LAB to W rest frame
      DO i=1,3
        Pboo(i)=-PW1(i)
      ENDDO
      CALL BOOST(Pboo,Pfb3,pdum1)
      CALL BOOST(Pboo,Pb,pdum2)
      cos_lW1=-COSVEC(pdum1,pdum2)
      ENDIF

!     lW distribution for antitop

      IF (IB2 .EQ. 1) THEN
      Pboo(0)=PW2(0)                                ! LAB to W rest frame
      DO i=1,3
        Pboo(i)=-PW2(i)
      ENDDO
      CALL BOOST(Pboo,Pf4,pdum1)
      CALL BOOST(Pboo,Pbb,pdum2)
      cos_lW2=-COSVEC(pdum1,pdum2)
      ENDIF

!     cos_l1 distribution for top

      Pboo(0)=Ptrec(0)                                ! LAB to t rest frame
      DO i=1,3
        Pboo(i)=-Ptrec(i)
      ENDDO
      CALL BOOST(Pboo,Pfb3,pdum1)
      Pboo(0)=PQ1(0)                                ! LAB to t rest frame
      DO i=1,3
        Pboo(i)=-PQ1(i)
      ENDDO
      CALL BOOST(Pboo,Ptrec,pdum2)
      cos_l1=COSVEC(pdum1,pdum2)

!     cos_l2 distribution for antitop

      Pboo(0)=Ptrec2(0)                                ! LAB to t rest frame
      DO i=1,3
        Pboo(i)=-Ptrec2(i)
      ENDDO
      CALL BOOST(Pboo,Pf4,pdum1)
      Pboo(0)=PQ2(0)                                ! LAB to t rest frame
      DO i=1,3
        Pboo(i)=-PQ2(i)
      ENDDO
      CALL BOOST(Pboo,Ptrec2,pdum2)
      cos_l2=COSVEC(pdum1,pdum2)

!     l+ distribution for top

      Pboo(0)=PB1(0)                                ! LAB to W rest frame
      DO i=1,3
        Pboo(i)=-PB1(i)
      ENDDO
      CALL BOOST(Pboo,Pfb1,pdum1)
      CALL BOOST(Pboo,Pb,pdum2)
      cos_lZ1=-COSVEC(pdum1,pdum2)

!     l+ distribution for antitop

      Pboo(0)=PB2(0)                                ! LAB to W rest frame
      DO i=1,3
        Pboo(i)=-PB2(i)
      ENDDO
      CALL BOOST(Pboo,Pfb2,pdum1)
      CALL BOOST(Pboo,Pbb,pdum2)
      cos_lZ2=-COSVEC(pdum1,pdum2)

!      var(10)=cos_lZ1
!      var(11)=cos_lZ2

      DO i=1,nvar
      val=var(i)
      nbin=INT((val-varmin(i))/(varmax(i)-varmin(i))*
     &  FLOAT(nbintot(i)))+1
      IF (nbin .LT. 1) nbin=1
      IF (nbin .GT. nbintot(i)) nbin=nbintot(i)
      varsig(i,nbin)=varsig(i,nbin)+WT
      ENDDO

      IF (IB1 .EQ. 1) THEN
        quant(1)=cos_lW1                 ! A_FB
        quant(2)=cos_lW1                 ! A+
        quant(3)=cos_lW1                 ! A-
      ENDIF

      IF (IB2 .EQ. 1) THEN
        quant(4)=cos_lW2                 ! A_FB
        quant(5)=cos_lW2                 ! A+
        quant(6)=cos_lW2                 ! A-
      ENDIF

      DO i=1,n_a
        IF (quant(i) .GT. asym_cent(i)) THEN
          SIGp(i)=SIGp(i)+WT
          SIG2p(i)=SIG2p(i)+WT**2
        ELSE IF (quant(i) .LT. asym_cent(i)) THEN
          SIGm(i)=SIGm(i)+WT
          SIG2m(i)=SIG2m(i)+WT**2
        ENDIF
      ENDDO
      RETURN
      END







