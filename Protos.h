#ifndef _Protos_h_
#define _Protos_h_
struct ProtosIncoming{
  int pid;
  int color1;
  int color2;
  double pz;
};
struct ProtosOutgoing{
  int pid;
  int color1;
  int color2;
  double p[3];
};
  


struct ProtosEvent{
  int    eventNumber;
  double weight;
  double Q;
  ProtosIncoming incoming[2];
  ProtosOutgoing outgoing[5];
};
#endif

